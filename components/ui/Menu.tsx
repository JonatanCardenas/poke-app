import * as React from 'react';
import Box from '@mui/material/Box';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { AreaChartOutlined, GitlabOutlined, HomeOutlined, InboxOutlined, InfoOutlined, MailOutlined, MenuOutlined, SettingOutlined, TeamOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { theme } from 'antd';
import Image from 'next/image';
import { Col, Row } from 'antd';
import pokeImage from '../../utils/images/pokeapi_256.3fa72200.png';
const { useToken } = theme;

interface Menu{
    menu:string;
    menuIcon:JSX.Element;
}


export default function SwipeableTemporaryDrawer() {
  const { token } = useToken();

  const [state, setState] = React.useState({
    left: false,
  });

  const MenuItems:Menu[] = [
    {menu:'Pokedex',menuIcon:<GitlabOutlined />},
    {menu:'Mi equipo',menuIcon: <TeamOutlined />},
    {menu:'Estadísticas',menuIcon: <AreaChartOutlined /> },
    {menu:'Configuración',menuIcon: <SettingOutlined /> },
    {menu:'Acerca de',menuIcon: <InfoOutlined />}
  ];

  const toggleDrawer =
    (anchor: 'left' , open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  const list = (anchor: 'left') => (
    <Box
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {MenuItems.map((item, index) => (
          <ListItem key={item.menu} disablePadding>
            <ListItemButton>
              <ListItemIcon >
                
                {item.menuIcon}
              </ListItemIcon>
              <ListItemText primary={item.menu} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      
    </Box>
  );

  return (
    <div>
      {(['left'] as const).map((anchor) => (
        <React.Fragment key={anchor} >
        <Row justify="end" align="middle">
          <Col span={12}>
             <Button 
              type='link'
              onClick={toggleDrawer(anchor, true)}
              style={{margin:'10px'}}>
              <MenuOutlined style={{color:token.colorPrimary}} />
              </Button>
          </Col>
          <Col span={12} style={{textAlign:'end'}}>
                 <Image src={pokeImage}  alt='logo pokemón'  
                  style={{ width: '115px', height:'auto',margin:'5px' }}/>
          </Col>
        </Row>
         
          <SwipeableDrawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            onOpen={toggleDrawer(anchor, true)}
          >
            {list(anchor)}
          </SwipeableDrawer>
        </React.Fragment>
      ))}
    </div>
  );
}