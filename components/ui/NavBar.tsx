import React from 'react'
import Menu from './Menu'
import { theme } from 'antd';
const { useToken } = theme;

export const NavBar = () => {
  const { token } = useToken();
  return (
    <div style={{background:token.colorBgContainer}}> 
        <Menu  />
    </div>
  )
}
