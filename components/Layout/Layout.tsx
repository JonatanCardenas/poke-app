
import Head from "next/head"
import { title } from "process";
import { NavBar } from "../ui";
import { theme } from "antd";
interface Props{
    title?:string;
}
const { useToken } = theme;

export const Layout = ({ title ='pokeApi', children }) => {
  const { token } = useToken();
  return (
        <div style={{background:token.colorBgBase, height:'100vh' }}>
             <Head>
                <title>{title}</title>
            </Head>
            <NavBar   />
            <p>jalando</p>
        </div>
          
  )
}

