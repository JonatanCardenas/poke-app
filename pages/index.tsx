import { theme } from "antd";
import { NavBar } from "../components/ui";
import { Layout } from "../components/Layout";

const { useToken } = theme;

export default function HomePage() {
  const { token } = useToken();
  return (
    <Layout > 
      <NavBar />
            Hola mundo
    </Layout>
  )
}