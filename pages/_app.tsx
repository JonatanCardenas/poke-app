import { ConfigProvider,theme } from "antd"
import { darkTheme } from "../themes"


function MyApp({ Component, pageProps }) {
  return(
    <ConfigProvider
    theme={{
      token: {
        colorPrimary: 'white',
        colorBgContainer:'#d53b47',
        colorBgBase: '#2b292c'
      },
      components:{
        Button:{
          colorPrimary:'white'
        },
      }
     }} >
        <Component {...pageProps} />
    </ConfigProvider>
  )
}
export default MyApp
